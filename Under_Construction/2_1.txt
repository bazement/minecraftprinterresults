
{
    label = "2_1",
    emitRedstone = true,
    buttonMode = true,
    shapes={
        {0, 0, 14, 1, 3, 15, texture="wool_colored_yellow"},
        {1, 0, 14, 2, 3, 15, texture="wool_colored_yellow"},
        {2, 0, 14, 3, 3, 15, texture="wool_colored_yellow"},
        {3, 0, 14, 4, 2, 15, texture="wool_colored_yellow"},
        {4, 0, 14, 5, 2, 15, texture="wool_colored_yellow"},
        {5, 0, 14, 6, 2, 15, texture="wool_colored_yellow"},
        {6, 0, 14, 7, 2, 15, texture="wool_colored_yellow"},
        {7, 14, 14, 8, 16, 15, texture="wool_colored_yellow"},
        {7, 0, 14, 8, 1, 15, texture="wool_colored_yellow"},
        {8, 13, 14, 9, 16, 15, texture="wool_colored_yellow"},
        {8, 0, 14, 9, 1, 15, texture="wool_colored_yellow"},
        {9, 12, 14, 10, 16, 15, texture="wool_colored_yellow"},
        {9, 0, 14, 10, 1, 15, texture="wool_colored_yellow"},
        {10, 10, 14, 11, 16, 15, texture="wool_colored_yellow"},
        {10, 0, 14, 11, 1, 15, texture="wool_colored_yellow"},
        {11, 9, 14, 12, 16, 15, texture="wool_colored_yellow"},
        {11, 0, 14, 12, 1, 15, texture="wool_colored_yellow"},
        {12, 8, 14, 13, 16, 15, texture="wool_colored_yellow"},
        {13, 7, 14, 14, 16, 15, texture="wool_colored_yellow"},
        {14, 6, 14, 15, 16, 15, texture="wool_colored_yellow"},
        {15, 5, 14, 16, 16, 15, texture="wool_colored_yellow"},
    }
}
                