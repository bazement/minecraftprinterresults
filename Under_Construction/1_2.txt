
{
    label = "1_2",
    emitRedstone = true,
    buttonMode = true,
    shapes={
        {0, 15, 14, 1, 16, 15, texture="wool_colored_yellow"},
        {1, 15, 14, 2, 16, 15, texture="wool_colored_yellow"},
        {2, 15, 14, 3, 16, 15, texture="wool_colored_yellow"},
        {3, 15, 14, 4, 16, 15, texture="wool_colored_yellow"},
        {4, 14, 14, 5, 16, 15, texture="wool_colored_yellow"},
        {5, 14, 14, 6, 16, 15, texture="wool_colored_yellow"},
        {6, 14, 14, 7, 16, 15, texture="wool_colored_yellow"},
        {7, 14, 14, 8, 16, 15, texture="wool_colored_yellow"},
        {8, 14, 14, 9, 16, 15, texture="wool_colored_yellow"},
        {9, 13, 14, 10, 16, 15, texture="wool_colored_yellow"},
        {10, 13, 14, 11, 16, 15, texture="wool_colored_yellow"},
        {11, 13, 14, 12, 16, 15, texture="wool_colored_yellow"},
        {12, 13, 14, 13, 16, 15, texture="wool_colored_yellow"},
        {13, 12, 14, 14, 16, 15, texture="wool_colored_yellow"},
        {14, 12, 14, 15, 16, 15, texture="wool_colored_yellow"},
        {15, 12, 14, 16, 16, 15, texture="wool_colored_yellow"},
    }
}
                