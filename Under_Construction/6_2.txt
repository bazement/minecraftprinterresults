
{
    label = "6_2",
    emitRedstone = true,
    buttonMode = true,
    shapes={
        {0, 0, 14, 1, 5, 15, texture="wool_colored_yellow"},
        {1, 0, 14, 2, 5, 15, texture="wool_colored_yellow"},
        {2, 0, 14, 3, 5, 15, texture="wool_colored_yellow"},
        {3, 0, 14, 4, 4, 15, texture="wool_colored_yellow"},
        {4, 0, 14, 5, 4, 15, texture="wool_colored_yellow"},
        {5, 0, 14, 6, 4, 15, texture="wool_colored_yellow"},
        {6, 0, 14, 7, 4, 15, texture="wool_colored_yellow"},
        {7, 0, 14, 8, 3, 15, texture="wool_colored_yellow"},
        {8, 0, 14, 9, 3, 15, texture="wool_colored_yellow"},
        {9, 0, 14, 10, 3, 15, texture="wool_colored_yellow"},
        {10, 0, 14, 11, 3, 15, texture="wool_colored_yellow"},
        {11, 0, 14, 12, 3, 15, texture="wool_colored_yellow"},
        {12, 0, 14, 13, 2, 15, texture="wool_colored_yellow"},
        {13, 0, 14, 14, 2, 15, texture="wool_colored_yellow"},
        {14, 0, 14, 15, 2, 15, texture="wool_colored_yellow"},
        {15, 0, 14, 16, 2, 15, texture="wool_colored_yellow"},
    }
}
                