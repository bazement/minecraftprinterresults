
{
    label = "4_3",
    emitRedstone = true,
    buttonMode = true,
    shapes={
        {0, 10, 14, 1, 16, 15, texture="wool_colored_black"},
        {0, 9, 14, 1, 10, 15, texture="wool_colored_red"},
        {0, 6, 14, 1, 9, 15, texture="wool_colored_black"},
        {0, 0, 14, 1, 6, 15, texture="wool_colored_red"},
        {1, 7, 14, 2, 16, 15, texture="wool_colored_black"},
        {1, 0, 14, 2, 7, 15, texture="wool_colored_red"},
        {2, 8, 14, 3, 16, 15, texture="wool_colored_black"},
        {2, 0, 14, 3, 8, 15, texture="wool_colored_red"},
        {3, 7, 14, 4, 16, 15, texture="wool_colored_black"},
        {3, 0, 14, 4, 7, 15, texture="wool_colored_red"},
        {4, 6, 14, 5, 16, 15, texture="wool_colored_black"},
        {4, 0, 14, 5, 6, 15, texture="wool_colored_red"},
        {5, 5, 14, 6, 16, 15, texture="wool_colored_black"},
        {5, 0, 14, 6, 5, 15, texture="wool_colored_red"},
        {6, 0, 14, 7, 16, 15, texture="wool_colored_black"},
        {7, 0, 14, 8, 16, 15, texture="wool_colored_black"},
        {8, 2, 14, 9, 16, 15, texture="wool_colored_black"},
        {8, 0, 14, 9, 2, 15, texture="wool_colored_red"},
        {9, 1, 14, 10, 16, 15, texture="wool_colored_black"},
        {9, 0, 14, 10, 1, 15, texture="wool_colored_red"},
        {10, 0, 14, 11, 16, 15, texture="wool_colored_black"},
        {11, 0, 14, 12, 16, 15, texture="wool_colored_black"},
        {12, 0, 14, 13, 16, 15, texture="wool_colored_black"},
        {13, 0, 14, 14, 16, 15, texture="wool_colored_black"},
        {14, 0, 14, 15, 16, 15, texture="wool_colored_black"},
        {15, 0, 14, 16, 16, 15, texture="wool_colored_black"},
    }
}
                