
{
    label = "3_3",
    emitRedstone = true,
    buttonMode = true,
    shapes={
        {0, 9, 14, 1, 16, 15, texture="wool_colored_black"},
        {0, 0, 14, 1, 9, 15, texture="wool_colored_red"},
        {1, 11, 14, 2, 16, 15, texture="wool_colored_black"},
        {1, 0, 14, 2, 11, 15, texture="wool_colored_red"},
        {2, 12, 14, 3, 16, 15, texture="wool_colored_black"},
        {2, 0, 14, 3, 12, 15, texture="wool_colored_red"},
        {3, 13, 14, 4, 16, 15, texture="wool_colored_black"},
        {3, 0, 14, 4, 13, 15, texture="wool_colored_red"},
        {4, 14, 14, 5, 16, 15, texture="wool_colored_black"},
        {4, 0, 14, 5, 14, 15, texture="wool_colored_red"},
        {5, 15, 14, 6, 16, 15, texture="wool_colored_black"},
        {5, 0, 14, 6, 15, 15, texture="wool_colored_red"},
        {6, 0, 14, 7, 16, 15, texture="wool_colored_red"},
        {7, 0, 14, 8, 16, 15, texture="wool_colored_red"},
        {8, 0, 14, 9, 16, 15, texture="wool_colored_red"},
        {9, 0, 14, 10, 16, 15, texture="wool_colored_red"},
        {10, 0, 14, 11, 16, 15, texture="wool_colored_red"},
        {11, 0, 14, 12, 16, 15, texture="wool_colored_red"},
        {12, 0, 14, 13, 16, 15, texture="wool_colored_red"},
        {13, 0, 14, 14, 16, 15, texture="wool_colored_red"},
        {14, 0, 14, 15, 16, 15, texture="wool_colored_red"},
        {15, 0, 14, 16, 16, 15, texture="wool_colored_red"},
    }
}
                