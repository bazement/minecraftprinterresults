
{
    label = "3_4",
    emitRedstone = true,
    buttonMode = true,
    shapes={
        {0, 6, 14, 1, 16, 15, texture="wool_colored_red"},
        {0, 0, 14, 1, 6, 15, texture="wool_colored_black"},
        {1, 11, 14, 2, 16, 15, texture="wool_colored_red"},
        {1, 0, 14, 2, 11, 15, texture="wool_colored_black"},
        {2, 15, 14, 3, 16, 15, texture="wool_colored_red"},
        {2, 0, 14, 3, 15, 15, texture="wool_colored_black"},
        {3, 0, 14, 4, 16, 15, texture="wool_colored_black"},
        {4, 0, 14, 5, 16, 15, texture="wool_colored_black"},
        {5, 0, 14, 6, 16, 15, texture="wool_colored_black"},
        {6, 0, 14, 7, 16, 15, texture="wool_colored_black"},
        {7, 0, 14, 8, 16, 15, texture="wool_colored_black"},
        {8, 0, 14, 9, 16, 15, texture="wool_colored_black"},
        {9, 0, 14, 10, 16, 15, texture="wool_colored_black"},
        {10, 0, 14, 11, 16, 15, texture="wool_colored_black"},
        {11, 0, 14, 12, 16, 15, texture="wool_colored_black"},
        {12, 0, 14, 13, 16, 15, texture="wool_colored_black"},
        {13, 0, 14, 14, 16, 15, texture="wool_colored_black"},
        {14, 0, 14, 15, 16, 15, texture="wool_colored_black"},
        {15, 0, 14, 16, 16, 15, texture="wool_colored_black"},
    }
}
                